﻿using System.Collections.Generic;
using System.Linq;
using System.IO;

using SaphetyStructure;
using SaphetyStructure.DB;
using System.Text;
using System;

    public static class WebApplicationMemory
    {
        //DB's
        private static TradePRD tradePrd = new TradePRD();
        private static TradeQA tradeQa = new TradeQA();
        private static TradeTST tradeTst = new TradeTST();

        private static readonly Dictionary<string, DestinationDB> dbs = new Dictionary<string, DestinationDB>() { { "Develop", tradeTst }, { "Quality", tradeQa }, { "Production", tradePrd } };
        private static string activeDestination = "Develop";

        //changes
        public static List<GroupChange> TradeAvailableChanges = Program.GetTradeChanges();
        public static List<GroupChange> NetdocsAvailableChanges = Program.GetNetdocsChanges();

        private static List<GroupChange> TradeSelectedChanges = new List<GroupChange>();
        private static List<GroupChange> NetdocsSelectedChanges = new List<GroupChange>();


        public static void Main(string[] args)
        {
            int i = 0;
        }

        public static void RefreshData()
        {
            //update Current status
            var s = Program.UpdateCurrentStatus();

            if (s != null)
            {
                TradeAvailableChanges = Program.GetTradeChanges();
                NetdocsAvailableChanges = Program.GetNetdocsChanges();
            }
            else
            {
                TradeAvailableChanges = new List<GroupChange>();
                NetdocsAvailableChanges = new List<GroupChange>();
            }

            TradeSelectedChanges = new List<GroupChange>();
            NetdocsSelectedChanges = new List<GroupChange>();
        }


    public static List<GroupChange> GetTradeAvailableChanges()
        {
            return TradeAvailableChanges ?? new List<GroupChange>();
        }

        public static List<GroupChange> GetNetdocsAvailableChanges()
        {
            return NetdocsAvailableChanges ?? new List<GroupChange>();
    }

        public static List<GroupChange> GetTradeSelectedChanges()
        {
            return TradeSelectedChanges ?? new List<GroupChange>();
    }

        public static List<GroupChange> GetNetdocsSelectedChanges()
        {
            return NetdocsSelectedChanges ?? new List<GroupChange>();
    }

        public static byte[] GenerateTradeChangeFile()
        {
            List<string> queries = TradeSelectedChanges.Select(group => group.changedTables.Select(table => table.GenerateInsertOrOverWriteQuery(SAPHETY_STRUCTURE.GetTableNameFromPartialName(SAPHETY_STRUCTURE.PLATFORM.TRADE, table.name))).ToList()).Aggregate((x1, x2) => { x1.AddRange(x2); return x1; });

            return Encoding.UTF8.GetBytes(queries.Aggregate((name1, name2) => name1 + "\n\n" + name2));
        }

        public static byte[] GenerateNetdocsChangeFile()
        {
            List<string> queries = NetdocsSelectedChanges.Select(group => group.changedTables.Select(table => table.GenerateInsertOrOverWriteQuery(SAPHETY_STRUCTURE.GetTableNameFromPartialName(SAPHETY_STRUCTURE.PLATFORM.NETDOCS, table.name))).ToList()).Aggregate((x1, x2) => { x1.AddRange(x2); return x1; });

            return Encoding.UTF8.GetBytes(queries.Aggregate((name1, name2) => name1 + "\n\n" + name2));
    }

        public static bool AddSelectedNetdocsChange(string groupName, string tableName, string changeKeysAsJson)
        {
            GroupChange srcGroup, group;
            TableChange srcTable, table;

            srcGroup = NetdocsAvailableChanges.Find(g => g.name.Equals(groupName));
            srcTable = srcGroup.changedTables.Find(t => t.name.Equals(tableName));

            if ((group = NetdocsSelectedChanges.Find(g => g.name.Equals(groupName))) == null)
            {
                group = new GroupChange(groupName);
            }

            if ((table = group.changedTables.Find(t => t.name.Equals(tableName))) == null)
            {
                table = new TableChange(tableName, srcTable.showName);
            }

            var change = srcTable.changes.Find(c => c.Equals(changeKeysAsJson));

            try
            {
                if (table.changes.FindIndex(c => c.Equals(changeKeysAsJson)) == -1)
                {
                    table.changes.Add(dbs[activeDestination].isChangeOverridingDestinationTable(table.name, change));
                }
                if (!group.changedTables.Contains(table))
                {
                    group.changedTables.Add(table);
                }
                if (!NetdocsSelectedChanges.Contains(group))
                {
                    NetdocsSelectedChanges.Add(group);
                }

                srcTable.changes.Remove(change);

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
    }

    public static bool AddSelectedTradeChange(string groupName, string tableName, string changeKeysAsJson)
        {
            GroupChange srcGroup, group;
            TableChange srcTable, table;

            srcGroup = TradeAvailableChanges.Find(g => g.name.Equals(groupName));
            srcTable = srcGroup.changedTables.Find(t => t.name.Equals(tableName));

            if ((group = TradeSelectedChanges.Find(g => g.name.Equals(groupName))) == null)
            {
                group = new GroupChange(groupName);
            }

            if((table = group.changedTables.Find(t => t.name.Equals(tableName))) == null)
            {
                table = new TableChange(tableName,srcTable.showName);
            }

            var change = srcTable.changes.Find(c => c.Equals(changeKeysAsJson));

            try
            {
                if (table.changes.FindIndex(c => c.Equals(changeKeysAsJson)) == -1)
                {
                    table.changes.Add(dbs[activeDestination].isChangeOverridingDestinationTable(table.name, change));
                }
                if (!group.changedTables.Contains(table))
                {
                    group.changedTables.Add(table);
                }
                if (!TradeSelectedChanges.Contains(group))
                {
                    TradeSelectedChanges.Add(group);
                }

                srcTable.changes.Remove(change);

                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public static void RemoveSelectedTradeChange(string groupName, string tableName, string stringifiedChange)
        {
            GroupChange gc = TradeSelectedChanges.Find(g => g.name.Equals(groupName));

            TableChange tc = gc.changedTables.Find(t => t.name.Equals(tableName));

            Change change = tc.changes.RemoveAndGet(c => c.ToString().Equals(stringifiedChange));

            TradeAvailableChanges.Find(g => g.name.Equals(groupName))
                .changedTables.Find(t => t.name.Equals(tableName))
                .changes.Add(change);

            if (tc.changes.Count == 0)
                gc.changedTables.Remove(tc);

            if (gc.changedTables.Count == 0)
                TradeSelectedChanges.Remove(gc);
        }

        public static void RemoveSelectedNetdocsChange(string groupName, string tableName, string stringifiedChange)
        {
            GroupChange gc = NetdocsSelectedChanges.Find(g => g.name.Equals(groupName));

            TableChange tc = gc.changedTables.Find(t => t.name.Equals(tableName));

            Change change = tc.changes.RemoveAndGet(c => c.ToString().Equals(stringifiedChange));

            NetdocsAvailableChanges.Find(g => g.name.Equals(groupName))
                .changedTables.Find(t => t.name.Equals(tableName))
                .changes.Add(change);

            if (tc.changes.Count == 0)
                gc.changedTables.Remove(tc);

            if (gc.changedTables.Count == 0)
                NetdocsSelectedChanges.Remove(gc);
        }

    public static void ChangeDestinationDB(string destinationID)
        {
            activeDestination = destinationID;

            //Update override state on trade and Netdocs changes
            if(TradeSelectedChanges.Count > 0)
            TradeSelectedChanges.Select(gc => gc.changedTables)
                                .Aggregate((lst1, lst2) => { lst1.AddRange(lst2); return lst1; })
                                .ForEach(tc => tc.changes.ForEach(ch => dbs[activeDestination].isChangeOverridingDestinationTable(tc.name, ch)));

            if (NetdocsSelectedChanges.Count > 0)
                NetdocsSelectedChanges.Select(gc => gc.changedTables)
                                .Aggregate((lst1, lst2) => { lst1.AddRange(lst2); return lst1; })
                                .ForEach(tc => tc.changes.ForEach(ch => dbs[activeDestination].isChangeOverridingDestinationTable(tc.name, ch)));
        }

        public static string GetCurrentDestinationDB()
        {
            return activeDestination;
        }
    }

    static class Auxiliary
    {
        // EXTENSION METHOD (AUXILIARY)
        public static T RemoveAndGet<T>(this List<T> list, Predicate<T> comparer)
        {
            lock (list)
            {
                T value = list.Find(comparer);
                list.Remove(value);
                return value;
            }
        }
    }