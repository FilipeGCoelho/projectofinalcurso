﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Oracle.ManagedDataAccess.Client;
using SaphetyStructure.DB;

namespace SaphetyStructure
{
    public class DestinationDB : DB.DB
    {
        public DestinationDB(string tableSpace, string DBName, string connectionString) : base(tableSpace, DBName, connectionString){}

        public Change isChangeOverridingDestinationTable(string tableName, Change change)
        {
            string realTableName = SAPHETY_STRUCTURE.GetTableNameFromPartialName(GetTableSpace().Equals("EIV_TRADE") ? SAPHETY_STRUCTURE.PLATFORM.TRADE : SAPHETY_STRUCTURE.PLATFORM.NETDOCS, tableName);

            string query = String.Format("select 'true' from {0} where {1}", realTableName, change.keys.Select(k => k.Key + " = '" + k.Value + '\'').Aggregate((s1, s2) => s1 + " and " + s2));

            change.conflict = ExecuteQuery(query, OverridingValidator);

            return change;
        }

        public bool OverridingValidator(OracleDataReader reader)
        {
            return reader.HasRows;
        }
    }
}
