﻿using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using SaphetyStructure;
using SaphetyStructure.DB;

namespace TriggerGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            TradeDB trade = new TradeDB();
            List<Table> trade_tables = SAPHETY_STRUCTURE.GetTradeGroups(trade, true)
                                                        .Select(gr => gr.tables)
                                                        .Aggregate((l1,l2)=>
                                                        {
                                                            l1.AddRange(l2);
                                                            return l1;
                                                        });

            VaultDB vault = new VaultDB(true);
            vault.BeginTransaction();

            //Tables
            trade_tables.ForEach(table => vault.ExecuteCommand(Utils.TemplateUtils.GenerateDropTable(table)));
            Debug.WriteLine("TRADE -> Tables droped");
            List<string> tables = trade_tables.Select(table => Utils.TemplateUtils.GenerateTable(table)).ToList();
            tables.ForEach(table => vault.ExecuteCommand(table));
            Debug.WriteLine("TRADE -> Tables generated");

            //Triggers
            List<string> triggers = trade_tables.Select(table => Utils.TemplateUtils.GenerateTrigger(table)).ToList();
            triggers.ForEach(trigger => vault.ExecuteCommand(trigger));
            Debug.WriteLine("TRADE -> Triggers generated");

            NetdocsDB netdocs = new NetdocsDB();
            List<Table> netdocs_tables = SAPHETY_STRUCTURE.GetNetdocsGroups(netdocs, true)
                                                        .Select(gr => gr.tables)
                                                        .Aggregate((l1, l2) =>
                                                        {
                                                            l1.AddRange(l2);
                                                            return l1;
                                                        });
            //Tables
            netdocs_tables.ForEach(table => vault.ExecuteCommand(Utils.TemplateUtils.GenerateDropTable(table)));
            Debug.WriteLine("NETDOCS -> Tables droped");
            tables = netdocs_tables.Select(table => Utils.TemplateUtils.GenerateTable(table)).ToList();
            tables.ForEach(table => vault.ExecuteCommand(table));
            Debug.WriteLine("NETDOCS -> Tables generated");

            //Triggers
            triggers = netdocs_tables.Select(table => Utils.TemplateUtils.GenerateTrigger(table)).ToList();
            triggers.ForEach(trigger => vault.ExecuteCommand(trigger));
            Debug.WriteLine("NETDOCS -> Triggers generated");

            vault.EndTransaction();
            vault.CloseConnection();
        }
    }
}
