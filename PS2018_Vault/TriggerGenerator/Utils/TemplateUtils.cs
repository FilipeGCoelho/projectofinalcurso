﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using SaphetyStructure.DB;

namespace TriggerGenerator.Utils
{
    /*  SQL TEMPLATE ARGS
     
        {0} TRIGGER NAME
        {1} ORIGIN TABLE NAME 
        {2} SELECT PRIMARY KEYS OLD
        {3} DESTINATION TABLE NAME
        {4} INSERT OLD KEYS
        {5} INSERT NEW KEYS
        {6} UPDATE NEW KEYS
        {7} UPDATE COMPARING KEYS 
        {8} SELECT OTHER KEYS
        {9} SELECT ALL KEYS
        {10} SELECT ALL KEYS TO NEW

     */
    public static class TemplateUtils
    { 
        private static String trigger_filePath = "TriggerTemplate.txt";
        private static String trigger_template = File.ReadAllText(trigger_filePath);

        private static String table_filePath = "TableTemplate.txt";
        private static String table_template = File.ReadAllText(table_filePath);


        //{0} TRIGGER NAME
        private static string GenerateTriggerName(Table table)
        {
            var toReturn = table.tableSpace.Substring(4) + '_' + table.name;
            return toReturn.Length > 30 ? toReturn.Substring(0,30) : toReturn;
        }

        //{1} ORIGIN TABLE NAME
        private static string GenerateOriginTableName(Table table)
        {
            return table.tableSpace + '.' + table.name;
        }

        //{2} SELECT PRIMARY KEYS OLD
        private static string GenerateSelectPrimaryKeysOld(Table table)
        {
            if(table.primary_keys.Count > 0)
                return "and " + table.primary_keys.Select(key => String.Format("{0} = :OLD.{0}", key.name))
                                     .Aggregate((string x, string y) => String.Format(" {0} AND {1}", new object[] { x, y }));

            return "";
                                     
        }

        //{3} DESTINATION TABLE NAME
        private static string GenerateDestinationTableName(Table table)
        {
            return GenerateTriggerName(table);
        }

        //{4} INSERT OLD KEYS
        private static string GenerateInsertOldKeys(Table table)
        {
            return table.GetAllKeys().Select(key => String.Format(":OLD.{0}", key.name))
                                     .Aggregate((string x, string y) => String.Format(" {0}, {1}", new object[] { x, y }));
        }

        //{5} INSERT NEW KEYS
        private static string GenerateInsertNewKeys(Table table)
        {
            return table.GetAllKeys().Select(key => String.Format(":NEW.{0}", key.name))
                                     .Aggregate((string x, string y) => String.Format(" {0}, {1}", new object[] { x, y }));
        }


        //{6} UPDATE NEW KEYS
        private static string GenerateUpdateNewKeys(Table table)
        {
            return table.GetAllKeys().Select(key => String.Format("{0} = :NEW.{0}", key.name))
                                    .Aggregate((string x, string y) => String.Format(" {0}, {1}", new object[] { x, y }));
        }


        //{7} UPDATE COMPARING KEYS 
        private static string GenerateOldAndNewKeyComparing(Table table)
        {
            if (table.other_keys.Count > 0) 
                return table.other_keys.Select(key => String.Format("((:OLD.{0} is null and :NEW.{0} is not null) or (:OLD.{0} is not null and :NEW.{0} is null) or (:OLD.{0} is not null and :NEW.{0} is not null and :OLD.{0} != :NEW.{0}))", key.name))
                                        .Aggregate((string x, string y) => String.Format("{0} OR {1}", new object[] { x, y }));

            return "TRUE";
        }


        //{8} SELECT OTHER KEYS
        private static string GenerateOtherKeys(Table table)
        {
            if (table.other_keys.Count > 0)
                return table.other_keys.Select(key => key.name)
                                        .Aggregate((string x, string y) => String.Format("{0}, {1}", new object[] { x, y }));

            return "*";
        }


        //{9} SELECT NEW OTHER KEYS
        private static string GenerateNewOtherKeys(Table table)
        {
                return table.GetAllKeys().Select(key => key.name)
                                     .Aggregate((string x, string y) => String.Format(" {0}, {1}", new object[] { x, y }));
        }

        //{10} SELECT ALL KEYS TO NEW
        private static string GenerateAllKeysComparedNew(Table table)
        {
            return "and " + table.GetAllKeys().Select(key => String.Format("{0} = :NEW.{0}", key.name))
                                     .Aggregate((string x, string y) => String.Format(" {0} AND {1}", new object[] { x, y }));
        }

        //{11} SELECT PRIMARY KEYS NEW
        private static string GenerateSelectPrimaryKeysNew(Table table)
        {
            if (table.primary_keys.Count > 0)
                return "and " + table.primary_keys.Select(key => String.Format("{0} = :NEW.{0}", key.name))
                                     .Aggregate((string x, string y) => String.Format(" {0} AND {1}", new object[] { x, y }));

            return "";

        }


        private static string GenerateTableColumns(Table table)
        {
            return table.GetAllKeys().Select(key => key.ToString())
                                     .Aggregate((string x, string y) => String.Format(" {0}, {1}", new object[] { x, y }));
        }

        private static string GeneratePrimaryKeys(Table table)
        {
            return table.primary_keys.Select(key => key.name)
                                     .Aggregate("",(string x, string y) => String.Format(" {0}, {1}", new object[] { x, y }));
        }

        public static string GenerateTrigger(Table table)
        {
            List<string> args = new List<string>();

            args.Add(GenerateTriggerName(table));
            args.Add(GenerateOriginTableName(table));
            args.Add(GenerateSelectPrimaryKeysOld(table));
            args.Add(GenerateDestinationTableName(table));
            args.Add(GenerateInsertOldKeys(table));
            args.Add(GenerateInsertNewKeys(table));
            args.Add(GenerateUpdateNewKeys(table));
            args.Add(GenerateOldAndNewKeyComparing(table));
            args.Add(GenerateOtherKeys(table));
            args.Add(GenerateNewOtherKeys(table));
            args.Add(GenerateAllKeysComparedNew(table));
            args.Add(GenerateSelectPrimaryKeysNew(table));

            return String.Format(trigger_template, args.ToArray());
        }

        internal static string GenerateTable(Table table)
        {
            List<string> args = new List<string>();

            args.Add(GenerateDestinationTableName(table));
            args.Add(GenerateTableColumns(table));
            args.Add(GeneratePrimaryKeys(table));

            return String.Format(table_template, args.ToArray());
        }

        internal static string GenerateDropTable(Table table)
        {
            return String.Format("DROP TABLE {0}", GenerateDestinationTableName(table));
        }
    }
}
