﻿using System;
using System.Collections.Generic;
using System.Linq;
using SaphetyStructure.DB;

namespace SaphetyStructure
{
    public static class SAPHETY_STRUCTURE
    {
        //TRADE
        static KeyValuePair<string, KeyValuePair<string,string>[]> PARTNER =         new KeyValuePair<string, KeyValuePair<string,string>[]>("PARTNER", new KeyValuePair<string, string>[] { new KeyValuePair<string, string>("PARTNER","PARTNER"), new KeyValuePair<string, string>("PARTNERCOMM","PARTNER COMMUNICATIONS"), new KeyValuePair<string, string>("PARTNERCERTEXCHANGE","PARTNER CERTIFICATES"), new KeyValuePair<string, string>("PARTNERMESSAGECONFIGURATION","PARTNER MESSAGE CONFIGURATION"), new KeyValuePair<string, string>("PARTNERCAPABILITY","PARTNER CAPABILITY") });
        static KeyValuePair<string, KeyValuePair<string,string>[]> CERTIFICATES =    new KeyValuePair<string, KeyValuePair<string,string>[]>("CERTIFICATE", new KeyValuePair<string, string>[] { new KeyValuePair<string, string>("CERTIFICATE", "CERTIFICATES") });
        static KeyValuePair<string, KeyValuePair<string,string>[]> SETTINGS =        new KeyValuePair<string, KeyValuePair<string,string>[]>("POOLING", new KeyValuePair<string, string>[] { new KeyValuePair<string, string>("POOLINGFILTERVALUE", "POOLING FILTER CONDITIONS"), new KeyValuePair<string, string>("POOLINGFILTERACTION","POOLING FILTER TYPE"), new KeyValuePair<string, string>("POOLING","POOLINGS"), new KeyValuePair<string, string>("POOLINGFILTER","POOLING FILTERS"), new KeyValuePair<string, string>("DEFINITIONGROUP","DEFINITION GROUPS"), new KeyValuePair<string, string>("PROTOCOL", "PROTOCOLS"), new KeyValuePair<string, string>("SYSTEMCONFS","SYSTEM CONFIGURATIONS") });
        static KeyValuePair<string, KeyValuePair<string,string>[]> INFOMESSAGE =     new KeyValuePair<string, KeyValuePair<string,string>[]>("INFOMESSAGE", new KeyValuePair<string, string>[] { new KeyValuePair<string, string>("INFOMESSAGEQUERY", "INFOMESSAGE QUERIES") });
        static KeyValuePair<string, KeyValuePair<string,string>[]> WORKFLOWS =       new KeyValuePair<string, KeyValuePair<string,string>[]>("WORKFLOWS", new KeyValuePair<string, string>[] { new KeyValuePair<string, string>("DOCUMENTWORKFLOWS", "WORKFLOW MAPPING"), new KeyValuePair<string, string>("WORKFLOWGROUP", "WORKFLOWS") });
        static KeyValuePair<string, KeyValuePair<string, string>[]> ADMINISTRATION = new KeyValuePair<string, KeyValuePair<string, string>[]>("ADMINISTRATION", new KeyValuePair<string, string>[] { new KeyValuePair<string, string>("TASKSCHEDULE", "SCHEDULE TASK PERIODICITY"), new KeyValuePair<string, string>("TASK", "SCHEDULE TASK"), new KeyValuePair<string, string>("TASKACTION", "SCHEDULE TASK ACTION") });
        static List<KeyValuePair<string, KeyValuePair<string, string>[]>> TRADE_GROUPS = new List<KeyValuePair<string, KeyValuePair<string, string>[]>>() { PARTNER, CERTIFICATES, SETTINGS, INFOMESSAGE, WORKFLOWS, ADMINISTRATION };

        //NETDOCS
        static KeyValuePair<string, KeyValuePair<string, string>[]> ENTITY = new KeyValuePair<string, KeyValuePair<string, string>[]>("ENTITY", new KeyValuePair<string, string>[] { new KeyValuePair<string, string>("ENTITY", "ENTITY"), new KeyValuePair<string, string>("ENTITYNOTIFICATION","ENTITIY NOTIFICATIONS"), new KeyValuePair<string, string>("ENTITYDOCUMENT", "ENTITY DOCUMENTS") });
        static KeyValuePair<string, KeyValuePair<string, string>[]> USERS = new KeyValuePair<string, KeyValuePair<string,string>[]>("USER", new KeyValuePair<string, string>[] { new KeyValuePair<string, string>("USERS", "USERS"), new KeyValuePair<string, string>("USERS2ROLE","USER ROLES"), new KeyValuePair<string, string>("USERS2NOTIF", "USERS TO NOTIFY"), new KeyValuePair<string, string>("WORKFLOWREMINDER", "WORKFLOW REMINDERS"), new KeyValuePair<string, string>("USERS2ENTITY", "ENTITY USERS")});
        static KeyValuePair<string, KeyValuePair<string, string>[]> RESOURCES = new KeyValuePair<string, KeyValuePair<string, string>[]>("RESOURCE", new KeyValuePair<string, string>[] { new KeyValuePair<string, string>("RESOURCES", "RESOURCES"), new KeyValuePair<string, string>("RESOURCEDATA", "RESOURCES DATA") });
        static List<KeyValuePair<string, KeyValuePair<string, string>[]>> NETDOCS_GROUPS = new List<KeyValuePair<string, KeyValuePair<string, string>[]>>() { ENTITY, USERS, RESOURCES };

        public enum PLATFORM { TRADE, NETDOCS }


        public static List<Group> GetTradeGroups(DB.DB db, bool isCalledByTriggerGenerator)
        {
            return TRADE_GROUPS.Select(kvp => new Group(kvp.Key, kvp.Value.Select(str => db.GetTable((isCalledByTriggerGenerator ? "" : "TRADE_") + str.Key, str.Value)).ToList()))
                                .ToList();
        }

        public static List<Group> GetNetdocsGroups(DB.DB db, bool isCalledByTriggerGenerator)
        {
            return NETDOCS_GROUPS.Select(kvp => new Group(kvp.Key, kvp.Value.Select(str => db.GetTable((isCalledByTriggerGenerator ? "" : "NETDOCS_") + str.Key, str.Value)).ToList()))
                                .ToList();
        }


        public static List<Table> GetTablesFromPartialName(PLATFORM platform, string name)
        {
            List<KeyValuePair<string,string>> tableNames;
            DB.DB db;

            if (platform == PLATFORM.TRADE)
            {
                tableNames = TRADE_GROUPS.Select(kvp => kvp.Value.ToList()).Aggregate((arr1, arr2) => { arr1.AddRange(arr2); return arr1; }).Where(kvp2 => kvp2.Key.Contains(name.Substring("TRADE_".Length))).ToList();

                db = new TradeDB();
            }
            else
            {
                tableNames = NETDOCS_GROUPS.Select(kvp => kvp.Value.ToList()).Aggregate((arr1, arr2) => { arr1.AddRange(arr2); return arr1; }).Where(kvp2 => kvp2.Key.Contains(name.Substring("NETDOCS_".Length))).ToList();

                db = new TradeDB();
            }

            return tableNames.Select(n => db.GetTable(n.Key,n.Value)).ToList();
        }

        public static String GetTableNameFromPartialName(PLATFORM platform, string name)
        {
            if (platform == PLATFORM.TRADE)
                return "EIV_TRADE." + TRADE_GROUPS.Select(kvp => kvp.Value.ToList()).Aggregate((arr1, arr2) => { arr1.AddRange(arr2); return arr1; }).Where(kvp2 => kvp2.Key.Contains(name.Substring("TRADE_".Length))).ToList().First().Key;

            else
                return "EIV_NETDOCS." + NETDOCS_GROUPS.Select(kvp => kvp.Value.ToList()).Aggregate((arr1, arr2) => { arr1.AddRange(arr2); return arr1; }).Where(kvp2 => kvp2.Key.Contains(name.Substring("NETDOCS_".Length))).ToList().First().Key;
        }
    }

}
