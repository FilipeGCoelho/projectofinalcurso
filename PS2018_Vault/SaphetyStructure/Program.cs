﻿using System;
using System.Collections.Generic;
using System.Text;
using SaphetyStructure.DB;

namespace SaphetyStructure
{
    public static class Program
    {
        private static VaultDB vault = new VaultDB(false);

        static void Main(string[] args)
        {
            List<GroupChange> list = GetTradeChanges();
            List<GroupChange> list2 = GetNetdocsChanges();
            Status status = UpdateCurrentStatus();
        }

        public static void CreateNewProject(string eid, string name)
        {
            vault.CreateNewProject(eid, name);
        }

        public static void DeleteCurrentProject()
        {
            vault.DeleteCurrentProject();
        }

        public static List<GroupChange> GetTradeChanges()
        {
            return vault.GetTradeChanges();
        }

        public static List<GroupChange> GetNetdocsChanges()
        {
            return vault.GetNetdocsChanges();
        }

        public static Status UpdateCurrentStatus()
        {
            return vault.GetUpdatedStatus();
        }

        public static List<Status> GetProjects()
        {
            return vault.GetProjects();
        }

        public static void ToggleIsTrackingOn()
        {
            vault.ToggleIsTrackingOn();
        }

        public static void ChangeMainProject(string eid)
        {
            vault.ChangeMainProject(eid);
        }
    }
}
