﻿    public static class Queries
    {
        public static readonly string SELECT_TABLE_NAMES = "select TABLE_NAME from user_tables order by table_name asc";

        public static readonly string SELECT_TABLE_PRIMARY_KEYS = "SELECT utc.COLUMN_NAME, utc.DATA_TYPE, utc.CHAR_LENGTH, utc.DATA_PRECISION FROM USER_TAB_COLUMNS utc, (SELECT DISTINCT cols.COLUMN_NAME, cols.TABLE_NAME FROM all_constraints cons, all_cons_columns cols WHERE cols.table_name = '{0}' AND cons.constraint_type = 'P' AND cons.constraint_name = cols.constraint_name AND cons.owner = cols.owner) pks WHERE utc.TABLE_NAME = '{0}' AND utc.COLUMN_NAME IN pks.COLUMN_NAME";

        public static readonly string SELECT_TABLE_OTHER_KEYS = "SELECT DISTINCT COLUMN_NAME, DATA_TYPE, CHAR_LENGTH, DATA_PRECISION FROM USER_TAB_COLUMNS WHERE table_name = '{0}' AND COLUMN_NAME NOT IN (SELECT DISTINCT cols.column_name FROM all_constraints cons, all_cons_columns cols WHERE cols.table_name = '{0}' AND cons.constraint_type = 'P' AND cons.constraint_name = cols.constraint_name AND cons.owner = cols.owner)";

        public static readonly string SELECT_VAULT_TABLES_MODIFICATIONS = "SELECT {0} FROM {1} WHERE VAULT_EID = '{2}' AND VAULT_TAG = '{3}'";
    }