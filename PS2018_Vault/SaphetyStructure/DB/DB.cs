﻿using System.Diagnostics;

using System;
using System.Collections.Generic;
using System.Linq;

using Oracle.ManagedDataAccess.Client;

namespace SaphetyStructure.DB
{
    public class DB
    {
        public String connectionString;
        public OracleConnection connection;
        public OracleTransaction transaction;
        public string tableSpace;

        public DB(string tableSpace, string DBName, string connectionString)
        {
            this.tableSpace = tableSpace;
            this.connectionString = connectionString;

            try
            {
                connection = new OracleConnection
                {
                    ConnectionString = connectionString
                };
                connection.Open();
            }
            catch (OracleException e)
            {
                Console.WriteLine("Problems when connecting to " + DBName + ": " + e.Message);
            }
        }

        public void CloseConnection()
        {
            connection.Close();
        }

        public void BeginTransaction()
        {
            this.transaction = connection.BeginTransaction();
        }

        public void EndTransaction()
        {
            this.transaction.Commit();
        }

        public void Rollback()
        {
            this.transaction.Rollback();
        }

        public List<string> GetTableNames()
        {
            List<string> table_names = new List<string>();

            //Prepare command query
            OracleCommand command = connection.CreateCommand();
            command.CommandText = Queries.SELECT_TABLE_NAMES;

            //Obtain result
            OracleDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                table_names.Add(reader.GetString(0));
            }

            return table_names;
        }

        private List<Column> GetPrimaryKeys(string tableName)
        {
            //Prepare command query
            OracleCommand command = connection.CreateCommand();
            command.CommandText = String.Format(Queries.SELECT_TABLE_PRIMARY_KEYS, tableName);
            
            return GetKeys(command);
        }

        private List<Column> GetOtherKeys(string tableName)
        { 
            //Prepare command query
            OracleCommand command = connection.CreateCommand();
            command.CommandText = String.Format(Queries.SELECT_TABLE_OTHER_KEYS, tableName);

            return GetKeys(command);
        }

        private List<Column> GetKeys(OracleCommand command)
        {
            List<Column> table_names = new List<Column>();

            //Obtain result
            OracleDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                string name = reader.GetString(0),
                       type = reader.GetString(1);

                string length = GetColumnLength(type, reader);

                table_names.Add(new Column(name,type,length));
            }

            return table_names;
        }

        public string GetColumnLength(string type, OracleDataReader reader)
        {
            string length = reader.IsDBNull(2) ? null : reader.GetInt32(2).ToString();
            string precision = reader.IsDBNull(3) ? "1" : reader.GetInt32(3).ToString();

            return String.Format(GetTypeTemplate(type), new object[] { length, precision });
        }

        public static string GetTypeTemplate(string type)
        {
            return TYPE_TEMPLATES.Where(pair => pair.Key.Equals(type)).FirstOrDefault().Value;
        }

        public List<Table> GetTables()
        {
            //GET TABLES' NAME
            List<string> table_names = GetTableNames();

            //GENERATE TABLES
            List<Table> tables = table_names.Select(name =>
            {
                List<Column> primary_keys = GetPrimaryKeys(name.Length > 30 ? name.Substring(0, 30) : name);
                List<Column> other_keys = GetOtherKeys(name.Length > 30 ? name.Substring(0, 30) : name);

                return new Table(tableSpace, name, name, primary_keys, other_keys);
            }).ToList();

            return tables;
        }

        public Table GetTable(string tableName, string showName)
        {
            List<Column> primary_keys = GetPrimaryKeys(tableName.Length > 30 ? tableName.Substring(0, 30) : tableName);
            List<Column> other_keys = GetOtherKeys(tableName.Length > 30 ? tableName.Substring(0, 30) : tableName);

            return new Table(tableSpace, tableName, showName, primary_keys, other_keys);
        }

        public string GetTableSpace()
        {
            return tableSpace;
        }

        private static readonly int NON_EXISTANT_EXCEPTION = 942;
        private static readonly int DUPLICATED_NAME_EXCEPTION = 955;
        private static readonly int INVALID_IDENTIFIER_EXCEPTION = 904;
        private static readonly int INVALID_TABLE_EXCEPTION = 903;
        private static readonly int ALREADY_EXISTANT_EXCEPTION = 04095;

        public void ExecuteCommand(String query)
        {
            OracleCommand command = connection.CreateCommand();
            command.CommandText = query;

            //Execute query with no retrieving (INSERT or UPDATE)
            try
            {
                command.ExecuteNonQuery();
            }catch (OracleException e)
            {
                if (e.Number != DUPLICATED_NAME_EXCEPTION && e.Number != NON_EXISTANT_EXCEPTION && e.Number != ALREADY_EXISTANT_EXCEPTION && e.Number != INVALID_IDENTIFIER_EXCEPTION && e.Number != INVALID_TABLE_EXCEPTION)
                {
                    this.transaction.Rollback();
                    this.transaction.Dispose();
                    throw e;
                }
            }
        }

        public T ExecuteQuery<T>(String query, Func<OracleDataReader,T> fnc)
        {
            if (connection.State == System.Data.ConnectionState.Closed)
            {
                connection.Open();
                this.transaction = connection.BeginTransaction();
            }

            OracleCommand command = connection.CreateCommand();
            command.CommandText = query;
            
            OracleDataReader reader =  command.ExecuteReader();
            
            return fnc(reader);
        }

        static readonly List<KeyValuePair<string, string>> TYPE_TEMPLATES = new List<KeyValuePair<string, string>>
        {
            new KeyValuePair<string,string>("VARCHAR2","({0})"),
            new KeyValuePair<string,string>("NUMBER","({1})"),
            new KeyValuePair<string,string>("VARCHAR","({0} CHAR)"),
            new KeyValuePair<string,string>("TIMESTAMP(6)",""),
            new KeyValuePair<string,string>("NVARCHAR2","({0})"),
            new KeyValuePair<string,string>("CLOB",""),
            new KeyValuePair<string,string>("NCLOB",""),
            new KeyValuePair<string,string>("CHAR","({0})"),
            new KeyValuePair<string,string>("DATE",""),
            new KeyValuePair<string,string>("XMLTYPE","")
        };
    }

    public class Table
    {
        public string tableSpace;
        public string name;
        public string showName;
        public List<Column> primary_keys;
        public List<Column> other_keys;

        public Table(string tableSpace, string name, string showName, List<Column> primaries, List<Column> others)
        {
            this.tableSpace = tableSpace;
            this.name = name;
            this.primary_keys = primaries;
            this.other_keys = others;
            this.showName = showName;

            Debug.WriteLine(String.Format("Table {0} generated: {1} keys, {2} other collumns",name, primaries.Count, others.Count));
        }

        public List<Column> GetAllKeys()
        {   
            
            return primary_keys.Count > 0 ? primary_keys.Concat(other_keys).ToList() : new List<Column> { new Column("*", null, null) };
        }

        internal string GetAllKeysToSelect()
        {
            return GetAllKeys().Select(col => col.name)
                                .Aggregate((col1, col2) => col1 + ", " + col2);
        }

        public TableChange GetChanges(DB db, Status status)
        {
            OracleCommand command = db.connection.CreateCommand();
            command.CommandText = String.Format(Queries.SELECT_VAULT_TABLES_MODIFICATIONS, GetAllKeysToSelect(), name.Length > 30 ? name.Substring(0,30) : name, status.eid, status.tag);
            Debug.WriteLine("Select realized on Table: {0},  QUERY: {1}", name, command.CommandText);
            OracleDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                List<Change> changes = new List<Change>();

                while (reader.Read())
                {
                    object[] cols = new object[other_keys.Count + primary_keys.Count];
                    reader.GetValues(cols);
                    //separate keys & others
                    List<Pair<string, object>> keys = cols.Take(primary_keys.Count)
                                                                  .Select((obj, idx) => new Pair<string, object>(primary_keys[idx].name, obj))
                                                                  .ToList();

                    List<Pair<string, object>> others = cols.Skip(primary_keys.Count)
                                                                  .Select((obj, idx) => new Pair<string, object>(other_keys[idx].name, obj))
                                                                  .ToList();

                    changes.Add(new Change(keys, others));
                }

                if(changes.Count > 0)
                    DeleteAnyIrrelevantUpdateFromDB(db, status, changes);

                return new TableChange(name, showName, changes);
            }

            return null;
        }

        private void DeleteAnyIrrelevantUpdateFromDB(DB db, Status status, List<Change> changes)
        {
            //GET EVERY BEFORE
            List<Change> befores = changes.Where(change => change.operation.Equals("BEFORE")).ToList();

            //MAP EVERY BEFORE TO AN AFTER AND VALIDATE IF IS A MATCH
            befores.Select(before => { before.operation = "AFTER"; return before; })
                    .Where(after =>  changes.FindAll(ch => ch.ToString().Equals(after.ToString())).Count > 1)
                    .ToList()
                    .ForEach(toDelete =>
                    {
                        string where = toDelete.keys
                                                .Select(key => String.Format("{0} = '{1}'", key.Key, key.Value))
                                                .Aggregate((s1, s2) => s1 + " and " + s2);

                        where += String.Format(" AND VAULT_EID = '{0}' AND VAULT_TAG = {1}", status.eid, status.tag);

                        OracleCommand command = db.connection.CreateCommand();
                        command.CommandText = String.Format("DELETE FROM {0} WHERE {1}", name, where);
                        command.ExecuteNonQuery();

                        changes.RemoveAll(ch => ch.ToString().Equals(toDelete.ToString()));
                    });
        }
    }

    public class Column
    {
        public string name;
        public string type;
        public string length;

        public Column (string name, string type, string length)
        {
            this.name = name;
            this.type = type;
            this.length = length;
        }

        public override string ToString()
        {
            return String.Format("{0} {1}{2}", name, type, length);
        }
    }

    public class Group
    {
        public string group;

        public List<Table> tables;

        public Group(string groupId, List<Table> tables)
        {
            this.group = groupId;
            this.tables = tables;
        }

        public List<TableChange> GetChanges(DB db, Status status)
        {
            return tables.Select(tb => tb.GetChanges(db, status)).ToList();
        }
    }
}
