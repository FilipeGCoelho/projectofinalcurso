﻿using System.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;
using Oracle.ManagedDataAccess.Client;

namespace SaphetyStructure.DB
{
    public class VaultDB : DB
    {
        private static readonly String GET_CURRENT_STATUS = "SELECT * FROM CURRENT_STATUS WHERE PROJECT_ACTIVE = 1";
        private static readonly String GET_PROJECTS = "SELECT * FROM CURRENT_STATUS";
        private new static String connectionString = "User Id=EIV_Vault;Password=123456; Data Source=localhost:1521/xe;";
        private Status status;
        private static List<Group> tradeGroups;
        private static List<Group> netdocsGroups;

        internal void CreateNewProject(string eid, string name)
        {
            OracleCommand command = connection.CreateCommand();

            //check if there's any active project (if not the new project will be the active project)
            command.CommandText = GET_CURRENT_STATUS;
            OracleDataReader reader = command.ExecuteReader();

            var newName = name.Replace("'", "''");

            command.CommandText = reader.HasRows ? string.Format("INSERT INTO CURRENT_STATUS VALUES (0,'{0}',1,'{1}',0)", eid, name.Replace("'", "''"))
                                                   : string.Format("INSERT INTO CURRENT_STATUS VALUES (1,'{0}',1,'{1}',1)", eid, name.Replace("'", "''"));

            command.ExecuteNonQuery();
        }

        internal void DeleteCurrentProject()
        {
            OracleTransaction transaction = connection.BeginTransaction();

            try
            {
                OracleCommand command = connection.CreateCommand();
                foreach (string name in this.GetTableNames().Where(name => !name.Equals("CURRENT_STATUS")))
                {
                    command.CommandText = string.Format("DELETE FROM {0} WHERE VAULT_EID = '{1}'", name, status.eid);
                    command.ExecuteNonQuery();
                }

                command.CommandText = string.Format("DELETE FROM CURRENT_STATUS WHERE EID = '{0}'", status.eid);
                command.ExecuteNonQuery();

                //DELETE FROM CURRENT_STATUS TABLE & CHANGE MAIN PROJECT
                command.CommandText = string.Format("DELETE FROM CURRENT_STATUS WHERE EID = '{0}'", status.eid);
                command.ExecuteNonQuery();
                command.CommandText = string.Format("UPDATE CURRENT_STATUS SET PROJECT_ACTIVE = 1, TRACKING_ON = 1 WHERE ROWNUM = 1", status.eid);
                command.ExecuteNonQuery();
                transaction.Commit();
            }
            catch (OracleException)
            {
                transaction.Rollback();
            }
            finally
            {
                transaction.Dispose();
            }
        }

        public VaultDB(bool isCalledByTriggerGenerator) : base("EIV_VAULT", "VaultDB", connectionString)
        {
            Status st = GetUpdatedStatus();
            tradeGroups = SAPHETY_STRUCTURE.GetTradeGroups(this, isCalledByTriggerGenerator);
            netdocsGroups = SAPHETY_STRUCTURE.GetNetdocsGroups(this, isCalledByTriggerGenerator);
        }

        internal void ToggleIsTrackingOn()
        {
            OracleCommand command = connection.CreateCommand();
            command.CommandText = "UPDATE CURRENT_STATUS SET TRACKING_ON = mod (TRACKING_ON + 1, 2) WHERE PROJECT_ACTIVE = 1";
            command.ExecuteNonQuery();
        }


        internal void ChangeMainProject(string eid)
        {
            OracleTransaction transaction = connection.BeginTransaction();

            try
            {
                OracleCommand command = connection.CreateCommand();
                command.CommandText = "update CURRENT_STATUS set TRACKING_ON = 0, PROJECT_ACTIVE = 0";
                command.ExecuteNonQuery();

                command.CommandText = String.Format("update CURRENT_STATUS set TRACKING_ON = 1, PROJECT_ACTIVE = 1 WHERE EID = '{0}'", eid);
                command.ExecuteNonQuery();
                transaction.Commit();
            }
            catch(OracleException)
            {
                transaction.Rollback();
            }
            finally
            {
                transaction.Dispose();
            }
        }

        public Status GetUpdatedStatus()
        {
            OracleCommand command = connection.CreateCommand();
            command.CommandText = GET_CURRENT_STATUS;
            OracleDataReader reader = command.ExecuteReader();

            reader.Read();

            if (reader.HasRows)
            {
                Status status = new Status(reader.GetInt32(2), reader.GetString(1), reader.GetInt32(0) == 1, reader.GetString(3), reader.GetInt32(4) == 1);
                this.status = status;
            }
            else this.status = null;

            return this.status;
        }

        public List<Status> GetProjects()
        {
            OracleCommand command = connection.CreateCommand();
            command.CommandText = GET_PROJECTS;
            OracleDataReader reader = command.ExecuteReader();

            List<Status> projects = new List<Status>();
            while (reader.Read())
            {
                Status status = new Status(reader.GetInt32(2), reader.GetString(1), reader.GetInt32(0) == 1, reader.GetString(3), reader.GetInt32(4) == 1);

                projects.Add(status);
            }
            return projects;
        }

        public List<GroupChange> GetTradeChanges()
        {
            if (status != null)
                return tradeGroups.Select(group => new GroupChange(group.group, group.GetChanges(this, status))).Where(gc => gc.HasChanges()).ToList();

            return new List<GroupChange>();
        }

        public List<GroupChange> GetNetdocsChanges()
        {
            if (status != null)
                return netdocsGroups.Select(group => new GroupChange(group.group, group.GetChanges(this, status))).Where(gc => gc.HasChanges()).ToList();

            return new List<GroupChange>();
        }

        public Status GetCurrentStatus()
        {
            return status;
        }
    }

    public class Status
    {
        public int tag;
        public string eid;
        public bool isTracking;
        public string name;
        public bool active;

        public Status(int tag, string eid, bool isTracking, string name, bool active)
        {
            this.tag = tag;
            this.eid = eid;
            this.isTracking = isTracking;
            this.name = name;
            this.active = active;
        }
    }

    public class GroupChange
    {
        public string name;
        public List<TableChange> changedTables;

        public GroupChange(string name, List<TableChange> changes)
        {
            this.name = name;
            this.changedTables = changes == null ? new List<TableChange>() : changes.Where(ch => ch != null).ToList();
        }

        public GroupChange(string name)
        {
            this.name = name;
            this.changedTables = new List<TableChange>();
        }

        public bool HasChanges()
        {
            return changedTables != null && changedTables.Count > 0;
        }
    }

    public class TableChange
    {
        public string name;
        public string showName;

        public List<Change> changes;

        public TableChange(string tableName, string showName)
        {
            this.name = tableName;
            this.showName = showName;
            this.changes = new List<Change>();
            Debug.WriteLine("Table change {0} created with {1} changes", tableName, changes.Count);
        }

        public TableChange(string tableName, string showName, List<Change> changes)
        {
            this.name = tableName;
            this.showName = showName;

            Debug.WriteLine("Table change {0} created with {1} changes", tableName, changes.Count);

            this.changes = RefactorChanges(changes);
        }

        private List<Change> RefactorChanges(List<Change> changes)
        {
            string[] notUpdateOperations = { "INSERT", "DELETE" };
            List<Change> toReturn = changes.Where(ch => notUpdateOperations.Contains(ch.operation)).ToList();

            Change[] updates = changes.Where(ch => ch.operation != null && !notUpdateOperations.Contains(ch.operation)).ToArray();
            for (int i = 0; i < updates.Length; i = i + 2)
            {
                toReturn.Add(new UpdateChange(updates[i].keys, updates[i].values, updates[i + 1].values));
            }

            return toReturn;
        }

        public string GenerateInsertOrOverWriteQuery(string realTableName)
        {
            return changes.Select(change => change.GenerateOutputQuery(realTableName)).Aggregate("--" + realTableName, (s1, s2) => s1 + "\n\n" + s2);
        }
    }

#pragma warning disable CS0659 // Type overrides Object.Equals(object o) but does not override Object.GetHashCode()
    public class Change
#pragma warning restore CS0659 // Type overrides Object.Equals(object o) but does not override Object.GetHashCode()
    {
        public string operation;

        public List<Pair<string, object>> keys;
        public List<Pair<string, object>> values;

        public bool conflict = false;

        public Change(List<Pair<string, object>> keys, List<Pair<string, object>> values)
        {
            string[] VAULT_COLLUMNS = new string[] { "VAULT_EID", "VAULT_TAG", "VAULT_TYPE" };

            if (keys.Where(key => key.Key.Equals("VAULT_TYPE")).Count() > 0)
                this.operation = keys.Where(key => key.Key.Equals("VAULT_TYPE")).First().Value.ToString();

            this.keys = keys.Where(key => !VAULT_COLLUMNS.Contains(key.Key)).ToList(); //skip all VAULT's key columns

            this.values = values;
        }

        public override bool Equals(object json)
        {
            if (json is string)
                return (json as string).Equals(ToString());

            return base.Equals(json);
        }

        public override string ToString()
        {
            var keys = this.keys.Count == 0 ? "" : this.keys.Select(key => (key.Key + ':' + key.Value))
                        .Aggregate((s1, s2) => s1 + ',' + s2);

            var values = this.values.Count == 0 ? "" : this.values.Select(key => (key.Key + ':' + key.Value))
                        .Aggregate((s1, s2) => s1 + ',' + s2);


            return '{' + keys + ',' + values + '}';
        }

        public string GenerateOutputQuery(string realTableName)
        {
            if (operation.Equals("DELETE"))
                return GenerateDeleteQuery(realTableName);

            if (values.Count > 0)
                return GenerateInsertOrUpdateQuery(realTableName);

            return GenerateInsertQuery(realTableName);
        }

        //{0} destination table name
        //{1} primary keys
        //{2} values
        //{3} insert (keys + values)
        private static readonly string INSERT_OR_OVERWRITE = "merge into {0} destiny using (select count(*) as nr from {0} where {1}) results on( 1 = results.nr) when matched then update set {2} where {1} when not matched then insert ({4},{5})values({3});";

        private string GenerateInsertOrUpdateQuery(string realTableName)
        {
            return string.Format(INSERT_OR_OVERWRITE, new object[]{ realTableName,
                                                                    keys.Select(k => k.Key + " = '" + k.Value + '\'').Aggregate((s1,s2) => s1 + " and " + s2),
                                                                    values.Select(k => k.Key + " = '" + k.Value + '\'').Aggregate((s1,s2) => s1 + ", " + s2),
                                                                    keys.Select(k => '\'' + k.Value.ToString() + '\'').Aggregate((s1,s2) => s1 + ", " + s2) + ", " + values.Select(k => '\'' + k.Value.ToString() + '\'').Aggregate((s1, s2) => s1 + ", " + s2),
                                                                    keys.Select(k => k.Key).Aggregate((s1,s2) => s1 + ", " + s2),
                                                                    values.Select(k => k.Key).Aggregate((s1, s2) => s1 + ", " + s2)});
        }

        //{0} destination table name
        //{1} primary keys
        private static readonly string DELETE = "delete from {0} where {1}";

        private string GenerateDeleteQuery(string realTableName)
        {
            return string.Format(DELETE, new object[]{ realTableName,
                                                        keys.Select(k => k.Key + " = '" + k.Value + '\'').Aggregate((s1,s2) => s1 + " and " + s2)});
        }


        //{0} destination table name
        //{1} insert (keys + values)
        private static readonly string INSERT = "insert into {0} values ({1})";

        private string GenerateInsertQuery(string realTableName)
        {
            return string.Format(INSERT, new object[]{  realTableName,
                                                        keys.Select(k => '\'' + k.Value.ToString() + '\'').Aggregate((s1,s2) => s1 + ", " + s2)});
        }
    }

    public class UpdateChange : Change
    {
        public List<Pair<string, object>> oldValues;

        public UpdateChange(List<Pair<string, object>> keys, List<Pair<string, object>> values, List<Pair<string, object>> oldValues) : base(keys, values)
        {
            this.oldValues = oldValues;
            base.operation = "UPDATE";
        }

        public List<Pair<string, Pair<object, object>>> GetDifferentColumns()
        {
            List<Pair<string, Pair<object, object>>> toReturn =

                values.Zip(oldValues, (nv, ov) =>
                {
                    if (!nv.Value.Equals(ov.Value))
                        return new Pair<string, Pair<object, object>>(nv.Key, new Pair<object, object>(nv.Value is DBNull ? "null" : nv.Value, ov.Value is DBNull ? "null" : ov.Value));

                    return null;
                }).ToList();

            return toReturn.Where(ele => ele != null).ToList();
        }

    }

    public class Pair<X, Y>
    {
        public X Key;
        public Y Value;

        public Pair(X key, Y value)
        {
            this.Key = key;
            this.Value = value;
        }
    }
}
