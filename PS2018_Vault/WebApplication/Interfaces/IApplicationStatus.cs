﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SaphetyStructure.DB;

namespace WebApplication.Interfaces
{
    public interface IApplicationStatus
    {
        Status GetCurrentStatus();

        List<Status> GetProjects();

        void ToggleStatusIsTrackingOn();

        void ChangeMainProject(string eid);

        void CreateNewProject(string eid, string name);

        void DeleteCurrentProject();
    }
}
