﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SaphetyStructure.DB;
using System.IO;

namespace WebApplication.Interfaces
{
    public interface IPlatformChangeRepository
    {
        List<GroupChange> availableChanges { get;}
        List<GroupChange> selectedChanges { get; }

        bool SelectChange(string groupName, string tableName, string stringifiedChange);

        void UnselectChange(string groupName, string tableName, string stringifiedChange);

        byte[] ProduceQueryFile();

        void changeDestinationDB(string newDestinationID);

        string GetCurrentDestinationDB();
    }
}
