﻿using System.Collections.Generic;
using SaphetyStructure.DB;
using SaphetyStructure;
using WebApplication.Interfaces;
using System.IO;

namespace WebApplication.Models
{
    public class TradeChangeRepository : ITradeChangeRepository
    {
        List<GroupChange> IPlatformChangeRepository.availableChanges => WebApplicationMemory.GetTradeAvailableChanges();

        List<GroupChange> IPlatformChangeRepository.selectedChanges => WebApplicationMemory.GetTradeSelectedChanges();

        public void changeDestinationDB(string newDestinationID)
        {
            WebApplicationMemory.ChangeDestinationDB(newDestinationID);
        }

        public string GetCurrentDestinationDB()
        {
            return WebApplicationMemory.GetCurrentDestinationDB();
        }

        public void RefreshData()
        {
            WebApplicationMemory.RefreshData();
        }

        public bool SelectChange(string groupName, string tableName, string stringifiedChange)
        {
            return WebApplicationMemory.AddSelectedTradeChange(groupName,tableName,stringifiedChange);
        }
        
        public void UnselectChange(string groupName, string tableName, string stringifiedChange)
        {
            WebApplicationMemory.RemoveSelectedTradeChange(groupName, tableName, stringifiedChange);
        }

        byte[] IPlatformChangeRepository.ProduceQueryFile()
        {
            return WebApplicationMemory.GenerateTradeChangeFile();
        }
    }
}
