﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SaphetyStructure.DB;
using SaphetyStructure;
using WebApplication.Interfaces;

namespace WebApplication.Models
{
    public class VaultStatus : IApplicationStatus
    {
        public void ChangeMainProject(string eid)
        {
            SaphetyStructure.Program.ChangeMainProject(eid);
        }

        public void CreateNewProject(string eid, string name)
        {
            SaphetyStructure.Program.CreateNewProject(eid, name);
        }

        public void DeleteCurrentProject()
        {
            SaphetyStructure.Program.DeleteCurrentProject();
        }

        public Status GetCurrentStatus()
        {
            return SaphetyStructure.Program.UpdateCurrentStatus();
        }

        public List<Status> GetProjects()
        {
            return SaphetyStructure.Program.GetProjects();
        }

        public void ToggleStatusIsTrackingOn()
        {
            SaphetyStructure.Program.ToggleIsTrackingOn();
        }
    }
}
