﻿using Microsoft.AspNetCore.Mvc;
using WebApplication.Interfaces;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApplication.Views.newProject
{
    public class NewProjectController : Controller
    {
        private IApplicationStatus statusRepo;

        public NewProjectController(IApplicationStatus statusRepo)
        {
            this.statusRepo = statusRepo;
        }

        public IActionResult Index()
        {
            ViewBag.Title = "New Project";
            return View();
        }

        public RedirectToActionResult SubmitData(string Name, string Eid)
        {
            statusRepo.CreateNewProject(Eid, Name);

            return new RedirectToActionResult("", "", null);
        }

    }
}
