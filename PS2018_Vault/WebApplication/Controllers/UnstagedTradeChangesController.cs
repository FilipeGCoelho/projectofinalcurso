﻿using System.Collections.Generic;
using SaphetyStructure.DB;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Interfaces;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApplication.Controllers
{
    public class UnstagedTradeChangesController : Controller
    {
        private ITradeChangeRepository tradeRepo;
        private IApplicationStatus statusRepo;

        public UnstagedTradeChangesController(ITradeChangeRepository changeRepo, IApplicationStatus statusRepo)
        {
            this.tradeRepo = changeRepo;
            this.statusRepo = statusRepo;
        }

        public ActionResult Index(bool IOException)
        {
            var status = statusRepo.GetCurrentStatus();
            if (status == null)
                return RedirectToAction("../newProject");

            ApplicationModel changes = new ApplicationModel(tradeRepo.selectedChanges, tradeRepo.availableChanges, status , IOException, statusRepo.GetProjects(), tradeRepo.GetCurrentDestinationDB());
            ViewBag.Title = "Trade";
            return View("UnstagedChanges",changes);
        }
        
        public RedirectToActionResult SelectChange(string groupName, string tableName, string stringifiedChange)
        {
            bool success = tradeRepo.SelectChange(groupName, tableName, stringifiedChange);

            return success ? new RedirectToActionResult("", "UnstagedTradeChanges", null) : new RedirectToActionResult("", "", new { IOException = true });
        }

        public RedirectToActionResult UnselectChange(string groupName, string tableName, string stringifiedChange)
        {
            tradeRepo.UnselectChange(groupName, tableName, stringifiedChange);

            return new RedirectToActionResult("", "UnstagedTradeChanges", null);
        }

        public RedirectToActionResult RefreshData()
        {
            tradeRepo.RefreshData();

            return new RedirectToActionResult("", "UnstagedTradeChanges", null);
        }

        public RedirectToActionResult ChangeMainProject(string eid)
        {
            statusRepo.ChangeMainProject(eid);
            tradeRepo.RefreshData();
            return new RedirectToActionResult("", "UnstagedTradeChanges", null);
        }

        public RedirectToActionResult DeleteCurrentProject()
        {
            statusRepo.DeleteCurrentProject();
            tradeRepo.RefreshData();
            return new RedirectToActionResult("", "UnstagedTradeChanges", null);
        }

        public FileResult ProduceQueryFile()
        {
            return new FileContentResult(tradeRepo.ProduceQueryFile(), "text/plain");
        }
        
        public void ToggleTrackingStatus()
        {
            statusRepo.ToggleStatusIsTrackingOn();
        }

        public RedirectToActionResult ChangeDestinationDb(string destinationID)
        {
            tradeRepo.changeDestinationDB(destinationID);

            return new RedirectToActionResult("", "UnstagedTradeChanges", null);
        }

        public Status GetStatus()
        {
            return statusRepo.GetCurrentStatus();
        }
    }

    public class ApplicationModel
    {
        public List<GroupChange> selected;
        public List<GroupChange> available;
        public Status status;
        public bool OracleException;
        public List<Status> projects;
        public string currentApplicationDb;

        public ApplicationModel(List<GroupChange> selected, List<GroupChange> available, Status status, bool OracleException, List<Status> projects, string currentApplicationDb)
        {
            this.selected = selected;
            this.available = available;
            this.status = status;
            this.OracleException = OracleException;
            this.projects = projects;
            this.currentApplicationDb = currentApplicationDb;
        }
    }

}
