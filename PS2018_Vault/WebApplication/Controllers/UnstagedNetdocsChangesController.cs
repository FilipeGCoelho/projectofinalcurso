﻿using Microsoft.AspNetCore.Mvc;
using SaphetyStructure.DB;
using WebApplication.Interfaces;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApplication.Controllers
{
    public class UnstagedNetdocsChangesController : Controller
    {
        private INetDocsChangeRepository netdocsRepo;
        private IApplicationStatus statusRepo;
        
        public IActionResult Index(bool IOException)
        {
            var status = statusRepo.GetCurrentStatus();
            if (status == null)
                return RedirectToAction("../newProject");

            ApplicationModel changes = new ApplicationModel(netdocsRepo.selectedChanges, netdocsRepo.availableChanges, status, IOException, statusRepo.GetProjects(), netdocsRepo.GetCurrentDestinationDB());
            ViewBag.Title = "Netdocs";
            return View("UnstagedChanges",changes);
        }

        

        public UnstagedNetdocsChangesController(INetDocsChangeRepository changeRepo, IApplicationStatus statusRepo)
        {
            this.netdocsRepo = changeRepo;
            this.statusRepo = statusRepo;
        }

        public RedirectToActionResult SelectChange(string groupName, string tableName, string stringifiedChange)
        {
            bool success = netdocsRepo.SelectChange(groupName, tableName, stringifiedChange);

            return success ? new RedirectToActionResult("", "UnstagedNetdocsChanges", null) : new RedirectToActionResult("", "", new { IOException = true });
        }

        public RedirectToActionResult UnselectChange(string groupName, string tableName, string stringifiedChange)
        {
            netdocsRepo.UnselectChange(groupName, tableName, stringifiedChange);

            return new RedirectToActionResult("", "UnstagedNetdocsChanges", null);
        }

        public RedirectToActionResult RefreshData()
        {
            netdocsRepo.RefreshData();

            return new RedirectToActionResult("", "UnstagedNetdocsChanges", null);
        }

        public RedirectToActionResult ChangeMainProject(string eid)
        {
            statusRepo.ChangeMainProject(eid);
            netdocsRepo.RefreshData();
            return new RedirectToActionResult("", "UnstagedNetdocsChanges", null);
        }

        public RedirectToActionResult DeleteCurrentProject()
        {
            statusRepo.DeleteCurrentProject();
            netdocsRepo.RefreshData();
            return new RedirectToActionResult("", "UnstagedNetdocsChanges", null);
        }

        public FileResult ProduceQueryFile()
        {
            return new FileContentResult(netdocsRepo.ProduceQueryFile(), "text/plain");
        }

        public void ToggleTrackingStatus()
        {
            statusRepo.ToggleStatusIsTrackingOn();
        }

        public RedirectToActionResult ChangeDestinationDb(string destinationID)
        {
            netdocsRepo.changeDestinationDB(destinationID);

            return new RedirectToActionResult("", "UnstagedNetdocsChanges", null);
        }

        public Status GetStatus()
        {
            return statusRepo.GetCurrentStatus();
        }
    }
}
